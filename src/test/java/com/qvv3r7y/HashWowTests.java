package com.qvv3r7y;

import com.qvv3r7y.model.Hash;
import com.qvv3r7y.service.HashService;
import javassist.NotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HashWowTests {

    @Test
    public void contextLoads() throws NotFoundException {
        Hash hash = new Hash("hello", "md5");
        HashService service = new HashService();
        service.computeHash(hash);
        Assert.assertEquals("5D41402ABC4B2A76B9719D911017C592", hash.getResult());
    }

    @Autowired
    private TestRestTemplate restTemplate;

}
