package com.qvv3r7y.service;

import com.qvv3r7y.model.Password;
import com.qvv3r7y.utils.PasswordGenerator;
import javassist.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.stream.Stream;

public class PasswordService {

    public void generatePassword(Password password) throws NotFoundException {
        switch (password.getType().toLowerCase(Locale.ROOT)) {
            case "random":
                generateRandomPassword(password);
                break;
            case "pin":
                generatePin(password);
                break;
            case "memorable":
                generateMemorablePassword(password);
                break;
            default:
                throw new NotFoundException("Incompatible type: " + password.getType().toLowerCase(Locale.ROOT));
        }
    }

    private void generateRandomPassword(Password password) {
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useSymbols(password.isSymbols())
                .useDigits(password.isNumbers())
                .useLower(true)
                .useUpper(true)
                .build();
        String pswd = passwordGenerator.generate(password.getLength());
        password.setGeneratedPassword(pswd);
    }

    private void generatePin(Password password) {
        int length = password.getLength();
        StringBuilder pin = new StringBuilder();

        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(32);
        random.setSeed(seed);

        for (int i = 0; i < length; i++) {
            int num = random.nextInt(10);
            pin.append(num);
        }

        password.setGeneratedPassword(pin.toString());
    }

    private void generateMemorablePassword(Password password) {
        final char SEPARATOR = '_';
        int length = password.getLength();
        StringBuilder pswd = new StringBuilder();

        SecureRandom random = new SecureRandom();
        byte[] seed = random.generateSeed(32);
        random.setSeed(seed);

        for (int i = 0; i < length; i++) {
            int num = random.nextInt(1770);
            try (Stream<String> lines = Files.lines(Path.of("src/main/resources/words.txt"))) {
                String word = lines.skip(num).findFirst().get();
                pswd.append(word).append(SEPARATOR);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        pswd.setLength(pswd.length() - 1);
        password.setGeneratedPassword(pswd.toString());
    }
}
