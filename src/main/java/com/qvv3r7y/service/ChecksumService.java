package com.qvv3r7y.service;

import com.qvv3r7y.model.Checksum;
import com.qvv3r7y.utils.DataTypeConverter;
import javassist.NotFoundException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.zip.CRC32;

public class ChecksumService {

    public void checkChecksum(Checksum checksum) throws NotFoundException {
        computeChecksum(checksum);

        if (checksum.getCheck() != null && checksum.getCheck().equals(checksum.getChecksum())) {
            checksum.setEquals("SUCCESS :)");
        } else {
            checksum.setEquals("FAIL :(");
        }
    }

    public void computeChecksum(Checksum checksum) throws NotFoundException {
        byte[] file = checksum.getData();
        switch (checksum.getAlgorithm().toUpperCase(Locale.ROOT)) {
            case "MD5":
                try {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    byte[] digest = md.digest(file);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    checksum.setChecksum(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA1":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-1");
                    byte[] digest = md.digest(file);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    checksum.setChecksum(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA256":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    byte[] digest = md.digest(file);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    checksum.setChecksum(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA512":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] digest = md.digest(file);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    checksum.setChecksum(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "CRC32":
                CRC32 crc32 = new CRC32();
                crc32.update(file);
                String result = Long.toHexString(crc32.getValue());
                checksum.setChecksum(result);
                break;
            default:
                throw new NotFoundException(checksum.getAlgorithm() + " Algorithm not found");
        }
    }
}
