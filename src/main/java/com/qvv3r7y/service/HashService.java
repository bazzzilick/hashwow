package com.qvv3r7y.service;

import com.qvv3r7y.model.Hash;
import com.qvv3r7y.utils.DataTypeConverter;
import javassist.NotFoundException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.zip.CRC32;

public class HashService {

    public void computeHash(Hash hash) throws NotFoundException {
        byte[] text = hash.getText().getBytes(StandardCharsets.UTF_8);
        switch (hash.getAlgorithm()) {
            case "MD5":
                try {
                    MessageDigest md = MessageDigest.getInstance("MD5");
                    byte[] digest = md.digest(text);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    hash.setResult(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "MD2":
                try {
                    MessageDigest md = MessageDigest.getInstance("MD2");
                    byte[] digest = md.digest(text);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    hash.setResult(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA1":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-1");
                    byte[] digest = md.digest(text);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    hash.setResult(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA256":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-256");
                    byte[] digest = md.digest(text);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    hash.setResult(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "SHA512":
                try {
                    MessageDigest md = MessageDigest.getInstance("SHA-512");
                    byte[] digest = md.digest(text);
                    String result = DataTypeConverter.getHexFromBytes(digest);
                    hash.setResult(result);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                break;
            case "CRC32":
                CRC32 crc32 = new CRC32();
                crc32.update(text);
                String result = Long.toHexString(crc32.getValue());
                hash.setResult(result);
                break;
            default:
                throw new NotFoundException(hash.getAlgorithm() + " Algorithm not found");
        }
    }

    public void encodeText(Hash code) throws NotFoundException {
        byte[] text = code.getText().getBytes(StandardCharsets.UTF_8);
        String result;
        Charset charset;
        switch (code.getAlgorithm()) {
            case "BASE64":
                String encodedText = Base64.getEncoder().encodeToString(text);
                code.setResult(encodedText);
                break;
            case "ASCII-8BIT":
                result = Arrays.toString(code.getText().getBytes(StandardCharsets.US_ASCII));
                code.setResult(result);
                break;
            case "UTF16":
                result = Arrays.toString(code.getText().getBytes(StandardCharsets.UTF_16));
                code.setResult(result);
                break;
            case "ISO8859-1":
                result = Arrays.toString(code.getText().getBytes(StandardCharsets.ISO_8859_1));
                code.setResult(result);
                break;
            case "UTF8":
                result = Arrays.toString(code.getText().getBytes(StandardCharsets.UTF_8));
                code.setResult(result);
                break;
            case "KOI8-R":
                charset = Charset.forName("KOI8-R");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "CP850":
                charset = Charset.forName("CP850");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "CP950":
                charset = Charset.forName("CP950");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "IBM852":
                charset = Charset.forName("IBM852");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "IBM869":
                charset = Charset.forName("IBM869");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "WINDOWS-1250":
                charset = Charset.forName("Windows-1250");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "WINDOWS-1251":
                charset = Charset.forName("Windows-1251");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "WINDOWS-1252":
                charset = Charset.forName("Windows-1252");
                result = new String(text, charset);
                code.setResult(result);
                break;
            case "WINDOWS-1253":
                charset = Charset.forName("Windows-1253");
                result = new String(text, charset);
                code.setResult(result);
                break;
            default:
                throw new NotFoundException(code.getAlgorithm() + " Algorithm not found");
        }
    }
}



