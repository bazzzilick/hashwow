package com.qvv3r7y.controller;

import com.qvv3r7y.model.Hash;
import com.qvv3r7y.service.HashService;
import javassist.NotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Locale;

@Controller
@RequestMapping("/hash")
public class HashController {

    @GetMapping
    public String hash() {
        return "../static/hash";
    }

    @PostMapping
    public String computeHash(@RequestParam String text, @RequestParam String algorithm, @RequestParam String codeAlgorithm, @RequestParam(defaultValue = "hash", required = false) String type, Model model) {
        Hash hash = new Hash(text, (type.toLowerCase(Locale.ROOT).equals("hash") ? algorithm : codeAlgorithm).toUpperCase(Locale.ROOT));

        HashService service = new HashService();
        try {
            if (type.toLowerCase(Locale.ROOT).equals("hash")) {
                service.computeHash(hash);
            } else if (type.toLowerCase(Locale.ROOT).equals("code")) {
                service.encodeText(hash);
            }

        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        model.addAttribute("hash", hash);
        return "hash";
    }
}
