package com.qvv3r7y.controller;

import com.qvv3r7y.model.Password;
import com.qvv3r7y.service.PasswordService;
import javassist.NotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/password")
public class PasswordController {

    @GetMapping
    public String password() {
        return "../static/password";
    }

    @PostMapping
    public String generatePassword(@RequestParam String type, @RequestParam(defaultValue = "10") int length, @RequestParam(defaultValue = "false", required = false) boolean symbols, @RequestParam(defaultValue = "false", required = false) boolean numbers, Model model) {
        Password password = new Password(type, length, symbols, numbers);
        PasswordService service = new PasswordService();
        try {
            service.generatePassword(password);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }

        model.addAttribute("password", password);

        return "password";
    }

}
